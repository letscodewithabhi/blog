$(function() {
    $(window).load(function () {
        $.ajax({
            url: 'landscapes.json',
            dataType: 'json',
            success: function(data) {

                function arrayColumn(array, columnName) {
                    return array.map(function(value,index) {
                        return value[columnName];
                    })
                }

                var images = arrayColumn(data.album, "img");

                $('#background').css({'background-image': 'url(' + images[Math.floor(Math.random() * images.length)] + ')'});

                //User Profile
                $('#profile_picture').attr("src", data.profile_picture);
                $('#name').append(data.name);
                $('#bio').append(data.bio);
                $('#phone'  ).append(data.phone);
                $('#email').append(data.email);

                $.each(data.album, function (key, val) {

                    if(key == 0){
                        $('#img').attr("src", val.img);
                        $('#title').append(val.title);
                        $('#description').append(val.description);
                        $('#date').append(val.date);
                        // if(val.featured == 1){
                        //     $('#featured').append('&#10084;');
                        // }
                    }else{
                        var $clone = $('#card-row').clone();
                        $clone.find('#img').prop('src', val.img);
                        $clone.find('#title').text(val.title);
                        $clone.find('#description').text(val.description);
                        $clone.find('#date').text(val.date);
                        // if(val.featured == 1){
                        //     $clone.find('#featured').html('&#10084;');
                        // }else{
                        //     $clone.find('#featured').remove();
                        // }
                        $clone.appendTo('#card-columns');
                    }
                });

            },
            statusCode: {
                404: function() {
                    alert('There was a problem with the server.  Try again soon!');
                }
            }
        });
    });
});