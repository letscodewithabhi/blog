<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    protected $table = "user";

    protected $primaryKey = "id";

    protected $fillable = [
        'name', 'email', 'password', "phone", "bio", "profile_picture"
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function album()
    {
        return $this->hasMany(Album::class);
    }
}
