<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Album extends Model
{
    use Notifiable;

    protected $table = "album";

    protected $primaryKey = "id";

    protected $fillable = [
        'title', 'description', 'img', "date", "featured", "user_id"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
