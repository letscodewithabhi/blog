Please make sure to run **composer update** on the project after cloning

If the **.env** file is missing please make sure to copy one from Laravel source code and run php artisan key:generate and place it against **APP_KEY**

Finally, run 
    **php artisan serve**

If the broswer doesn't open automatically please go to **localhost:8000**

Step While Development
1. Created a new project using laravel 7.x named blog

2. Copied over the required images and json file to read data from

3. Used bootstrap cards layout to mimic the wireframe.jpeg

4. Used Ajax to read the data in landscapes.json
	
	4.1 Select random images on each reload for background blurred image
	
	4.2 Looped through album and profile data to push and clone the DOM elements with Json data

5. Designed the Database strcuture in the code (Please note i have only designed the structure of database in the code and not the actual database, I have that on my local SQL server though)

6. Designed the controller to return just the actual JSON data as per the landscapes.json